\contentsline {section}{فهرست مطالب}{1}{section*.1}
\contentsline {section}{فهرست تصاویر}{2}{section*.2}
\contentsline {section}{\numberline {1}مقدمه}{4}{section.1}
\contentsline {section}{\numberline {2}کارهای پیشین}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}تحلیل مصرف}{6}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}حملات بدخواهانه}{11}{subsection.2.2}
\contentsline {section}{\numberline {3}چالش‌های موجود}{13}{section.3}
\contentsline {section}{\numberline {4}اهداف تحقیق}{15}{section.4}
\contentsline {section}{\numberline {5}مفروضات و محدودیت‌ها}{17}{section.5}
\contentsline {section}{\numberline {6}راهکارهای پیشنهادی}{18}{section.6}
\contentsline {section}{\numberline {7}روش انجام تحقیق}{21}{section.7}
\contentsline {section}{\numberline {8}ارزیابی راهکار پیشنهادی}{22}{section.8}
\contentsline {section}{\numberline {9}جنبه‌ی جديد بودن و نوآوری}{23}{section.9}
\contentsline {section}{\numberline {10}زمان‌بندی انجام تحقیق}{24}{section.10}
\contentsline {section}{مراجع}{25}{section*.4}
